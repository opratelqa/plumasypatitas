package pages; 


import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.sun.javafx.collections.MappingChange.Map;

import okhttp3.Call;
import test.TestBase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBasePlumasYPatitas;

//import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


 
public class PlumasYPatitas extends TestBasePlumasYPatitas {
	
	final WebDriver driver;
	public PlumasYPatitas(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 	
	
	 
	/*
	 ******PASAR A BASEPAGE 
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
	public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("loader"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	
	
	
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "number")
	private WebElement IngresaLineaTelefonica;
	
	@FindBy(how = How.ID,using = "acepto")
	private WebElement checkMayorDeEdad;
	
	@FindBy(how = How.ID,using = "si")
	private WebElement btnContinue;
	
	

	
	
	//************CONSTRUCTOR***********
		
	@FindBy(how = How.ID,using = "RankBorrar")
	private WebElement aProbar;

	//*****************************

	
	public void logInPlumasYPatitas(String apuntaA) {		
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test Plumas Y Patitas - landing");
		
		WebElement menu1 = driver.findElement(By.xpath("//a[contains(text(), 'Inicio')]"));
			menu1.click();
			espera(500);
				String elemento1 = driver.findElement(By.cssSelector(".section-heading")).getText();			
					Assert.assertEquals(elemento1,"Blog");
					System.out.println(elemento1);
			espera(500);
			
		WebElement menu2 = driver.findElement(By.xpath("//a[contains(text(), 'Blog')]"));
			menu2.click();
			espera(500);
				String elemento2 = driver.findElement(By.cssSelector(".section-heading")).getText();
					Assert.assertEquals(elemento2,"Blog");
					System.out.println(elemento2);
			espera(500);		
					
		WebElement menu3 = driver.findElement(By.xpath("//a[contains(text(), 'Adóptame')]"));
			menu3.click();
			espera(500);
				String elemento3 = driver.findElement(By.xpath("/html/body/section[1]/div/div/div/header/h1")).getText();
				Assert.assertEquals(elemento3,"Adóptame");
				System.out.println(elemento3);
			espera(500);
			
		WebElement menu4 = driver.findElement(By.xpath("//a[contains(text(), 'Descargas')]"));
			menu4.click();
			espera(500);
				String elemento4 = driver.findElement(By.cssSelector(".entry-title")).getText();
				Assert.assertEquals(elemento4,"Descargas");
				System.out.println(elemento4);
			espera(500);
			
		WebElement menu5 = driver.findElement(By.xpath("//a[contains(text(), 'Reglamentos')]"));
			menu5.click();
			espera(500);
				String elemento5 = driver.findElement(By.cssSelector(".col-md-12.text-center")).getText();
				Assert.assertEquals(elemento5,"Reglamentos");
				System.out.println(elemento5);
			espera(500);
						
		System.out.println("Resultado Esperado:Deberan visualizarce las secciones de Plumas Y Patitas"); 
		System.out.println();
		System.out.println("Fin de Test Patitas Plumas Y Patitas - Landing");
			
	}			

	
}  

